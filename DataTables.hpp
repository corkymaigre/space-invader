#ifndef DATATABLES_HPP
#define DATATABLES_HPP
//----------------------------------------------------------------------------------------------------------------------------------------------

#include "ResourceIdentifiers.hpp"
//----------------------------------------------------------------------------------------------------------------------------------------------

#include <SFML/System/Time.hpp>
#include <SFML/Graphics/Color.hpp>
//----------------------------------------------------------------------------------------------------------------------------------------------

#include <vector>
#include <functional>
//----------------------------------------------------------------------------------------------------------------------------------------------

class Spacecraft;
//----------------------------------------------------------------------------------------------------------------------------------------------

struct Direction
{
	Direction(float angle, float distance)
	: angle(angle)
	, distance(distance)
	{
	}

	float angle;
	float distance;
};
//----------------------------------------------------------------------------------------------------------------------------------------------

struct SpacecraftData // Single table entry
{
	int								hitpoints;
	float							speed;
	Textures::ID					texture;
	sf::Time						fireInterval;
	std::vector<Direction>			directions;
};
//----------------------------------------------------------------------------------------------------------------------------------------------

struct ProjectileData
{
	int								damage;
	float							speed;
	Textures::ID					texture;
};
//----------------------------------------------------------------------------------------------------------------------------------------------

struct PickupData
{
	std::function<void(Spacecraft&)>	action;
	Textures::ID					texture;
};
//----------------------------------------------------------------------------------------------------------------------------------------------

std::vector<SpacecraftData>	initializeSpacecraftData(); // The whole table with a sequence of entries
std::vector<ProjectileData>	initializeProjectileData();
std::vector<PickupData>		initializePickupData();
//----------------------------------------------------------------------------------------------------------------------------------------------
#endif // DATATABLES_HPP
