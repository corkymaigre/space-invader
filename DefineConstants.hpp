#ifndef DEFINECONSTANTS_HPP
#define DEFINECONSTANTS_HPP
//--------------------------------------------------------------------------


//------------------------------------------------------------------------------------------------------------------------------------------
//						WINDOW
//------------------------------------------------------------------------------------------------------------------------------------------

#define GAME_TITLE                  "Tintin Escaped The Red Planet"
#define WIN_WIDTH                   800
#define WIN_HEIGHT                  600
#define TIME_PER_FRAME				1.f/60.f
#define SCROLL_SPEED        		-100.f


//------------------------------------------------------------------------------------------------------------------------------------------
//						STATES
//------------------------------------------------------------------------------------------------------------------------------------------

#define TITLE_SCREEN				"Media/Textures/States/TitleScreen.jpg"
#define MENU_SCREEN					"Media/Textures/States/MenuScreen.jpg"
#define PAUSE_SCREEN				"Media/Textures/States/PauseScreen.jpg"
#define SETTINGS_SCREEN				"Media/Textures/States/SettingsScreen.jpg"
#define INSTRUCTIONS_SCREEN         "Media/Textures/States/InstructionsScreen.jpg"
#define ABOUT_SCREEN                "Media/Textures/States/AboutScreen.jpg"
#define HELP_SCREEN                 "Media/Textures/States/HelpScreen.jpg"


#define BUTTON_NORMAL       		"Media/Textures/ButtonNormal.png"
#define BUTTON_PRESSED      		"Media/Textures/ButtonPressed.png"
#define BUTTON_SELECTED     		"Media/Textures/ButtonSelected.png"


//------------------------------------------------------------------------------------------------------------------------------------------
//						PLAYER
//------------------------------------------------------------------------------------------------------------------------------------------

#define MOON_ROCKET                 "Media/Textures/Spacecrafts/MoonRocket.png"
#define PLAYER_SPEED                5.f
#define POS_INIT_X                  WIN_WIDTH/2
#define POS_INIT_Y                  WIN_HEIGHT-(WIN_HEIGHT/3)
#define ALLIED_BULLET       		"Media/Textures/AlliedBullet.png"


//------------------------------------------------------------------------------------------------------------------------------------------
//						ENEMIES
//------------------------------------------------------------------------------------------------------------------------------------------

#define NORMAL_SAUCER            	"Media/Textures/Spacecrafts/NormalSaucer.png"
#define ARMY_SAUCER            		"Media/Textures/Spacecrafts/ArmySaucer.png"
#define BLACK_SAUCER                "Media/Textures/Spacecrafts/BlackSaucer.png"
#define SPEED_SAUCER                "Media/Textures/Spacecrafts/SpeedSaucer.png"
#define ENEMY_BULLET        		"Media/Textures/EnemyBullet.png"



//------------------------------------------------------------------------------------------------------------------------------------------
//						PICKUPS
//------------------------------------------------------------------------------------------------------------------------------------------

#define HEALTH_REFILL       		"Media/Textures/HealthRefill.png"


//------------------------------------------------------------------------------------------------------------------------------------------
//						FONTS
//------------------------------------------------------------------------------------------------------------------------------------------

#define TITLE_FONT                  "Media/Fonts/Isocpeur.ttf"
#define MESSAGE_FONT                "Media/Fonts/Sansation.ttf"
#define TEXT_FONT                   "Media/Fonts/StylusBT.ttf"


#define TITLE_FONT_SIZE             26
#define STATS_SIZE          		16
#define STATS_X             		10.f
#define STATS_Y             		10.f


//------------------------------------------------------------------------------------------------------------------------------------------
//						BACKGROUND
//------------------------------------------------------------------------------------------------------------------------------------------

#define GROUND     		            "Media/Textures/Backgrounds/Ground.jpg"


//------------------------------------------------------------------------------------------------------------------------------------------
//						SOUNDS & MUSICS
//------------------------------------------------------------------------------------------------------------------------------------------

#define MENU_THEME     		        "Media/Sounds/Themes/MenuTheme.ogg"
#define GAME_THEME     		        "Media/Sounds/Themes/GameTheme.ogg"

#define ALLIED_FIRE_SOUND			"Media/Sounds/Fires/AlliedFire.wav"
#define ENEMY_FIRE_SOUND			"Media/Sounds/Fires/EnemyFire.wav"

#define EXPLOSION_1_SOUND			"Media/Sounds/Explosions/4.wav"
#define EXPLOSION_2_SOUND			"Media/Sounds/Explosions/3.wav"

#define COLLECT_PICKUP_SOUND		"Media/Sounds/Pickups/Pickup.wav"

#define BUTTON_SOUND				"Media/Sounds/Buttons/Button.wav"

#define GAME_OVER_SOUND				"Media/Sounds/Messages/GameOver.wav"
#define YOU_WIN_SOUND				"Media/Sounds/Messages/YouWin.wav"

//------------------------------------------------------------------------------------------------------------------------------------------

#endif // DEFINECONSTANTS_HPP
//------------------------------------------------------------------------------------------------------------------------------------------
