﻿#include "HelpState.hpp"
#include "Button.hpp"
#include "Utility.hpp"
#include "ResourceHolder.hpp"
//------------------------------------------------------------------------------------------------------------------------------------------------

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/View.hpp>
//------------------------------------------------------------------------------------------------------------------------------------------------

HelpState::HelpState(StateStack& stack, Context context)
: State(stack, context)
, mBackgroundSprite()
, mText()
, mGUIContainer()
{
	sf::Texture& texture = context.textures->get(Textures::HelpScreen);
	sf::Vector2f windowSize(context.window->getSize());

	mBackgroundSprite.setTexture(texture);

	mText.setFont(context.fonts->get(Fonts::Texts));
	mText.setString("The menu is composed by:\n"
					" #  Play - play the game.\n"
                    " #  Settings - change the control keys.\n"
                    " #  Instructions - read the instructions.\n"
                    " #  About - about the game and author.\n"
                    " #  Help - display the help window.\n"
                    " #  Exit - quit the game.\n");
    //mInstructionsText.setCharacterSize(40);
	centerOrigin(mText);
	mText.setPosition(0.5f * windowSize.x, 0.4f * windowSize.y);

	//auto backButton = std::make_shared<GUI::Button>(*context.fonts, *context.textures, *context.sounds);
	auto backButton = std::make_shared<GUI::Button>(context);
	backButton->setPosition(WIN_WIDTH / 2, 450.f);
	backButton->setText("Back to menu");
	backButton->setCallback(std::bind(&HelpState::requestStackPop, this));

	// Or
	/*backButton->setCallback([this] ()
	{
		requestStackPop();
	});*/


	mGUIContainer.pack(backButton);
}
//------------------------------------------------------------------------------------------------------------------------------------------------

void HelpState::draw()
{
	sf::RenderWindow& window = *getContext().window;
	window.setView(window.getDefaultView());

	sf::RectangleShape backgroundShape;
	backgroundShape.setFillColor(sf::Color(0, 0, 0, 180));
	backgroundShape.setSize(window.getView().getSize());

	window.draw(mBackgroundSprite);
	window.draw(backgroundShape);
	window.draw(mText);
	window.draw(mGUIContainer);
}
//------------------------------------------------------------------------------------------------------------------------------------------------

bool HelpState::update(sf::Time)
{
	return true;
}
//------------------------------------------------------------------------------------------------------------------------------------------------

bool HelpState::handleEvent(const sf::Event& event)
{
	mGUIContainer.handleEvent(event);
	return false;
}
//------------------------------------------------------------------------------------------------------------------------------------------------
