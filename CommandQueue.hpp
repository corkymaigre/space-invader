#ifndef COMMANDQUEUE_HPP
#define COMMANDQUEUE_HPP
//--------------------------------------------------------------------------------------------------------------------------------------------------

#include "Command.hpp"
//--------------------------------------------------------------------------------------------------------------------------------------------------

#include <queue> 	// Pile FIFO
//--------------------------------------------------------------------------------------------------------------------------------------------------

class CommandQueue
{
	public:
		void								push(const Command& command);
		Command								pop();
		bool								isEmpty() const;


	private:
		std::queue<Command>					mQueue;
};
//--------------------------------------------------------------------------------------------------------------------------------------------------

#endif // COMMANDQUEUE_HPP
//--------------------------------------------------------------------------------------------------------------------------------------------------