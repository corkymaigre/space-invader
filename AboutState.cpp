#include "AboutState.hpp"
#include "Button.hpp"
#include "Utility.hpp"
#include "ResourceHolder.hpp"
//------------------------------------------------------------------------------------------------------------------------------------------------

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/View.hpp>
//------------------------------------------------------------------------------------------------------------------------------------------------

AboutState::AboutState(StateStack& stack, Context context)
: State(stack, context)
, mBackgroundSprite()
, mText()
, mGUIContainer()
{
	sf::Texture& texture = context.textures->get(Textures::AboutScreen);
	sf::Vector2f windowSize(context.window->getSize());

	mBackgroundSprite.setTexture(texture);

	mText.setFont(context.fonts->get(Fonts::Texts));
	mText.setString("This game has been implemented in C++11\n"
                    "with the SFML 2.0 library.\n"
                    "\nCoded by Corky Maigre for a school project\n");
	centerOrigin(mText);
	mText.setPosition(0.5f * windowSize.x, 0.4f * windowSize.y);

	//auto backButton = std::make_shared<GUI::Button>(*context.fonts, *context.textures, *context.sounds);
	auto backButton = std::make_shared<GUI::Button>(context);
	backButton->setPosition(WIN_WIDTH / 2, 450.f);
	backButton->setText("Back to menu");
	backButton->setCallback(std::bind(&AboutState::requestStackPop, this));

	// Or
	/*backButton->setCallback([this] ()
	{
		requestStackPop();
	});*/


	mGUIContainer.pack(backButton);
}
//------------------------------------------------------------------------------------------------------------------------------------------------

void AboutState::draw()
{
	sf::RenderWindow& window = *getContext().window;
	window.setView(window.getDefaultView());

	sf::RectangleShape backgroundShape;
	backgroundShape.setFillColor(sf::Color(0, 0, 0, 180));
	backgroundShape.setSize(window.getView().getSize());

	window.draw(mBackgroundSprite);
	window.draw(backgroundShape);
	window.draw(mText);
	window.draw(mGUIContainer);
}
//------------------------------------------------------------------------------------------------------------------------------------------------

bool AboutState::update(sf::Time)
{
	return true;
}
//------------------------------------------------------------------------------------------------------------------------------------------------

bool AboutState::handleEvent(const sf::Event& event)
{
	mGUIContainer.handleEvent(event);
	return false;
}
//------------------------------------------------------------------------------------------------------------------------------------------------
