#include "TitleState.hpp"
#include "Utility.hpp"
#include "ResourceHolder.hpp"
//------------------------------------------------------------------------------------------------------------------------------------------------

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
//------------------------------------------------------------------------------------------------------------------------------------------------

TitleState::TitleState(StateStack& stack, Context context)
: State(stack, context)
, mText()
, mShowText(true)
, mTextEffectTime(sf::Time::Zero)
{
	mBackgroundSprite.setTexture(context.textures->get(Textures::TitleScreen));

	mText.setFont(context.fonts->get(Fonts::Messages));
	mText.setString("Press any key to start");
	mText.setCharacterSize(38);
	centerOrigin(mText);
	mText.setPosition(sf::Vector2f(context.window->getSize() / 2u));
	// OR mText.setPosition(context.window->getView().getSize() / 2.f);
}
//------------------------------------------------------------------------------------------------------------------------------------------------

void TitleState::draw()
{
	sf::RenderWindow& window = *getContext().window;

	sf::RectangleShape backgroundShape;
	backgroundShape.setFillColor(sf::Color(0, 0, 0, 40));
	backgroundShape.setSize(window.getView().getSize());

	window.draw(mBackgroundSprite);
    window.draw(backgroundShape);

	if (mShowText)
		window.draw(mText);
}
//------------------------------------------------------------------------------------------------------------------------------------------------

bool TitleState::update(sf::Time dt)
{
	mTextEffectTime += dt;

	if (mTextEffectTime >= sf::seconds(0.5f))
	{
		mShowText = !mShowText;
		mTextEffectTime = sf::Time::Zero;
	}

	return true;
}
//------------------------------------------------------------------------------------------------------------------------------------------------

bool TitleState::handleEvent(const sf::Event& event)
{
	// If any key is pressed, trigger the next screen
	if (event.type == sf::Event::KeyReleased)
	{
		requestStackPop();
		requestStackPush(States::Menu);
	}

	return true;
}
//------------------------------------------------------------------------------------------------------------------------------------------------
