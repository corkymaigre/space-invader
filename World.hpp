#ifndef WORLD_HPP
#define WORLD_HPP
//----------------------------------------------------------------------------------------------------------------------------------------------

#include "ResourceHolder.hpp"
#include "ResourceIdentifiers.hpp"
#include "SceneNode.hpp"
#include "SpriteNode.hpp"
#include "Spacecraft.hpp"
#include "DefineConstants.hpp"
#include "CommandQueue.hpp"
#include "Command.hpp"
#include "SoundPlayer.hpp"
//----------------------------------------------------------------------------------------------------------------------------------------------

#include <SFML/System/NonCopyable.hpp>
#include <SFML/Graphics/View.hpp>
#include <SFML/Graphics/Texture.hpp>
//----------------------------------------------------------------------------------------------------------------------------------------------

#include <array>
#include <queue>
//----------------------------------------------------------------------------------------------------------------------------------------------

// Forward declaration
namespace sf
{
	class RenderWindow;
}
//----------------------------------------------------------------------------------------------------------------------------------------------

class World : private sf::NonCopyable
{
	public:
		explicit								World(sf::RenderWindow& window, FontHolder& fonts, SoundPlayer& sounds);
		void									update(sf::Time dt);
		void									draw();

		CommandQueue&							getCommandQueue();

		bool 									hasAlivePlayer() const;
		bool 									hasPlayerReachedEnd() const;


	private:
		void									loadTextures();
		void									adaptPlayerPosition();
		void									adaptPlayerVelocity();
		void									handleCollisions();
		void									updateSounds();

		void									buildScene();
		void									addEnemies();
		void									addEnemy(Spacecraft::Type type, float relX, float relY);
		void									spawnEnemies();
		void									destroyEntitiesOutsideView();
		sf::FloatRect							getViewBounds() const;
		sf::FloatRect							getBattlefieldBounds() const;


	private:
		enum Layer
		{
			Background,
			Space,
			LayerCount
		};

		struct SpawnPoint
		{
			SpawnPoint(Spacecraft::Type type, float x, float y)
			: type(type)
			, x(x)
			, y(y)
			{
			}

			Spacecraft::Type type;
			float x;
			float y;
		};


	private:
		sf::RenderWindow&						mWindow;
		sf::View								mWorldView;
		TextureHolder							mTextures;
		FontHolder&								mFonts;
		SoundPlayer&							mSounds;

		SceneNode								mSceneGraph;
		std::array<SceneNode*, LayerCount>		mSceneLayers;
		CommandQueue							mCommandQueue;

		sf::FloatRect							mWorldBounds;
		sf::Vector2f							mSpawnPosition;
		float									mScrollSpeed;
		Spacecraft*								mPlayerSpacecraft;

		std::vector<SpawnPoint>					mEnemySpawnPoints;
		std::vector<Spacecraft*>				mActiveEnemies;
};
//----------------------------------------------------------------------------------------------------------------------------------------------

#endif // WORLD_HPP
//--------------------------------------------------------------------------------------------------------------------------------------------------
