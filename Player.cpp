#include "Player.hpp"
#include "CommandQueue.hpp"
#include "Spacecraft.hpp"
//----------------------------------------------------------------------------------------------------------------------------------------------

#include <map>
#include <string>
#include <algorithm>
//----------------------------------------------------------------------------------------------------------------------------------------------

using namespace std::placeholders;
//----------------------------------------------------------------------------------------------------------------------------------------------

struct SpacecraftMover
{
	SpacecraftMover(float vx, float vy)
	: velocity(vx, vy)
	{
	}

	void operator() (Spacecraft& spacecraft, sf::Time) const
	{
		spacecraft.accelerate(velocity * spacecraft.getMaxSpeed());
	}

	sf::Vector2f velocity;
};
//----------------------------------------------------------------------------------------------------------------------------------------------

Player::Player()
: mCurrentGameStatus(GameRunning)
{
	// Set initial key bindings
	mKeyBinding[sf::Keyboard::Left] = MoveLeft;
	mKeyBinding[sf::Keyboard::Right] = MoveRight;
	mKeyBinding[sf::Keyboard::Up] = MoveUp;
	mKeyBinding[sf::Keyboard::Down] = MoveDown;
	mKeyBinding[sf::Keyboard::Space] = Fire;

	// Set initial action bindings
	initializeActions();

	// Assign all categories to player's spacecraft
	// FOREACH
	for(auto& pair : mActionBinding)
		pair.second.category = Category::PlayerSpacecraft;
}
//----------------------------------------------------------------------------------------------------------------------------------------------

void Player::handleEvent(const sf::Event& event, CommandQueue& commands)
{
	if (event.type == sf::Event::KeyPressed)
	{
		// Check if pressed key appears in key binding, trigger command if so
		auto found = mKeyBinding.find(event.key.code);
		if (found != mKeyBinding.end() && !isRealtimeAction(found->second))
			commands.push(mActionBinding[found->second]);
	}
}
//----------------------------------------------------------------------------------------------------------------------------------------------

void Player::handleRealtimeInput(CommandQueue& commands)
{
	// Traverse all assigned keys and check if they are pressed
	// FOREACH
	for(auto pair : mKeyBinding)
	{
		// If key is pressed, lookup action and trigger corresponding command
		if (sf::Keyboard::isKeyPressed(pair.first)
			&& isRealtimeAction(pair.second))
				commands.push(mActionBinding[pair.second]);
	}
}
//----------------------------------------------------------------------------------------------------------------------------------------------

void Player::assignKey(Action action, sf::Keyboard::Key key)
{
	// Remove all keys that already map to action
	for (auto itr = mKeyBinding.begin(); itr != mKeyBinding.end(); )
	{
		if (itr->second == action)
			mKeyBinding.erase(itr++);
		else
			++itr;
	}

	// Insert new binding
	mKeyBinding[key] = action;
}
//----------------------------------------------------------------------------------------------------------------------------------------------

sf::Keyboard::Key Player::getAssignedKey(Action action) const
{
	// FOREACH
	for(auto pair : mKeyBinding)
	{
		if (pair.second == action)
			return pair.first;
	}

	return sf::Keyboard::Unknown;
}
//----------------------------------------------------------------------------------------------------------------------------------------------

void Player::setGameStatus(GameStatus status)
{
	mCurrentGameStatus = status;
}
//----------------------------------------------------------------------------------------------------------------------------------------------

Player::GameStatus Player::getGameStatus() const
{
	return mCurrentGameStatus;
}
//----------------------------------------------------------------------------------------------------------------------------------------------

void Player::initializeActions()
{
	mActionBinding[MoveLeft].action	 	= derivedAction<Spacecraft>(SpacecraftMover(-1, 0));
	mActionBinding[MoveRight].action 	= derivedAction<Spacecraft>(SpacecraftMover(+1, 0));
	mActionBinding[MoveUp].action    	= derivedAction<Spacecraft>(SpacecraftMover(0, -1));
	mActionBinding[MoveDown].action  	= derivedAction<Spacecraft>(SpacecraftMover(0, +1));

	mActionBinding[Fire].action          = derivedAction<Spacecraft>([] (Spacecraft& a, sf::Time){ a.fire(); });
}
//----------------------------------------------------------------------------------------------------------------------------------------------

bool Player::isRealtimeAction(Action action)
{
	switch (action)
	{
		case MoveLeft:
		case MoveRight:
		case MoveDown:
		case MoveUp:
		case Fire:
			return true;

		default:
			return false;
	}
}
//----------------------------------------------------------------------------------------------------------------------------------------------
