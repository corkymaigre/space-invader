#include "DefineConstants.hpp"
#include "Application.hpp"
#include "Utility.hpp"
#include "State.hpp"
#include "StateIdentifiers.hpp"
#include "TitleState.hpp"
#include "GameState.hpp"
#include "MenuState.hpp"
#include "PauseState.hpp"
#include "SettingsState.hpp"
#include "InstructionsState.hpp"
#include "AboutState.hpp"
#include "HelpState.hpp"
#include "GameOverState.hpp"
#include "SoundPlayer.hpp"
//----------------------------------------------------------------------------------------------------------------------------------------------

const sf::Time Application::TimePerFrame = sf::seconds(TIME_PER_FRAME);
//----------------------------------------------------------------------------------------------------------------------------------------------------

Application::Application()
: mWindow(sf::VideoMode(WIN_WIDTH, WIN_HEIGHT), GAME_TITLE, sf::Style::Close)
, mTextures()
, mFonts()
, mPlayer()
, mMusic()
, mSounds()
, mStateStack(State::Context(mWindow, mTextures, mFonts, mPlayer, mMusic, mSounds))
, mStatisticsText()
, mStatisticsUpdateTime()
, mStatisticsNumFrames(0)
{
	mWindow.setKeyRepeatEnabled(false);

	// LOADIND FONTS
	mFonts.load(Fonts::Titles, 						TITLE_FONT);
	mFonts.load(Fonts::Messages,                    MESSAGE_FONT);
	mFonts.load(Fonts::Texts, 						TEXT_FONT);

	// LOADING STATE SCREENS
	mTextures.load(Textures::TitleScreen,		    TITLE_SCREEN);
	mTextures.load(Textures::MenuScreen,		    MENU_SCREEN);
	mTextures.load(Textures::PauseScreen,		    PAUSE_SCREEN);
	mTextures.load(Textures::SettingsScreen,		SETTINGS_SCREEN);
	mTextures.load(Textures::InstructionsScreen,    INSTRUCTIONS_SCREEN);
	mTextures.load(Textures::AboutScreen,           ABOUT_SCREEN);
	mTextures.load(Textures::HelpScreen,            HELP_SCREEN);
	mTextures.load(Textures::ButtonNormal,		    BUTTON_NORMAL);
	mTextures.load(Textures::ButtonSelected,	    BUTTON_SELECTED);
	mTextures.load(Textures::ButtonPressed,		    BUTTON_PRESSED);

	mStatisticsText.setFont(mFonts.get(Fonts::Messages));
	mStatisticsText.setPosition(10.f, 10.f);
	mStatisticsText.setCharacterSize(16);

	registerStates();

	// Application starts with the title screen
	mStateStack.pushState(States::Title);

	mMusic.setVolume(50.f);
}
//----------------------------------------------------------------------------------------------------------------------------------------------

void Application::run()
{
	sf::Clock clock;
	sf::Time timeSinceLastUpdate = sf::Time::Zero;

	while (mWindow.isOpen())
	{
		sf::Time dt = clock.restart();
		timeSinceLastUpdate += dt;
		while (timeSinceLastUpdate > TimePerFrame)
		{
			timeSinceLastUpdate -= TimePerFrame;

			processInput();
			update(TimePerFrame);

			// Check inside this loop, because stack might be empty before update() call
			if (mStateStack.isEmpty())
				mWindow.close();
		}

		updateStatistics(dt);
		render();
	}
}
//----------------------------------------------------------------------------------------------------------------------------------------------

void Application::processInput()
{
	sf::Event event;
	while (mWindow.pollEvent(event))
	{
		mStateStack.handleEvent(event);

		if (event.type == sf::Event::Closed)
			mWindow.close();
	}
}
//----------------------------------------------------------------------------------------------------------------------------------------------

void Application::update(sf::Time dt)
{
	mStateStack.update(dt);
}
//----------------------------------------------------------------------------------------------------------------------------------------------

void Application::render()
{
	mWindow.clear();

	mStateStack.draw();

	mWindow.setView(mWindow.getDefaultView());
	mWindow.draw(mStatisticsText);

	mWindow.display();
}
//----------------------------------------------------------------------------------------------------------------------------------------------

void Application::updateStatistics(sf::Time dt)
{
	mStatisticsUpdateTime += dt;
	mStatisticsNumFrames += 1;

	if (mStatisticsUpdateTime >= sf::seconds(1.0f))
	{
		mStatisticsText.setString(
			"Frames / Second = " + toString(mStatisticsNumFrames) + "\n" +
			"Time / Update = " + toString(mStatisticsUpdateTime.asMicroseconds() / mStatisticsNumFrames) + "us");

		mStatisticsUpdateTime -= sf::seconds(1.0f);
		mStatisticsNumFrames = 0;
	}
}
//----------------------------------------------------------------------------------------------------------------------------------------------

void Application::registerStates()
{
	mStateStack.registerState<TitleState>(States::Title);
	mStateStack.registerState<MenuState>(States::Menu);
	mStateStack.registerState<GameState>(States::Game);
	mStateStack.registerState<PauseState>(States::Pause);
	mStateStack.registerState<SettingsState>(States::Settings);
	mStateStack.registerState<InstructionsState>(States::Instructions);
	mStateStack.registerState<AboutState>(States::About);
	mStateStack.registerState<HelpState>(States::Help);
	mStateStack.registerState<GameOverState>(States::GameOver);
}
//----------------------------------------------------------------------------------------------------------------------------------------------
