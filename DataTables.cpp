#include "DataTables.hpp"
#include "Spacecraft.hpp"
#include "Projectile.hpp"
#include "Pickup.hpp"
//----------------------------------------------------------------------------------------------------------------------------------------------

// For std::bind() placeholders _1, _2, ...
using namespace std::placeholders;
//----------------------------------------------------------------------------------------------------------------------------------------------

// Or in YAML or XML or Lua !

std::vector<SpacecraftData> initializeSpacecraftData()
{
	std::vector<SpacecraftData> data(Spacecraft::TypeCount);

	data[Spacecraft::MoonRocket].hitpoints = 100;
	data[Spacecraft::MoonRocket].speed = 250.f;
	data[Spacecraft::MoonRocket].texture = Textures::MoonRocket;
	data[Spacecraft::MoonRocket].fireInterval = sf::seconds(1.5);

	data[Spacecraft::NormalSaucer].hitpoints = 30;
	data[Spacecraft::NormalSaucer].speed = 80.f;
	data[Spacecraft::NormalSaucer].texture = Textures::NormalSaucer;
	data[Spacecraft::NormalSaucer].directions.push_back(Direction(+45.f, 80.f));
	data[Spacecraft::NormalSaucer].directions.push_back(Direction(-45.f, 160.f));
	data[Spacecraft::NormalSaucer].directions.push_back(Direction(+45.f, 80.f));
	data[Spacecraft::NormalSaucer].fireInterval = sf::Time::Zero;

	data[Spacecraft::ArmySaucer].hitpoints = 40;
	data[Spacecraft::ArmySaucer].speed = 50.f;
	data[Spacecraft::ArmySaucer].texture = Textures::ArmySaucer;
	data[Spacecraft::ArmySaucer].directions.push_back(Direction(+45.f,  50.f));
	data[Spacecraft::ArmySaucer].directions.push_back(Direction(  0.f,  50.f));
	data[Spacecraft::ArmySaucer].directions.push_back(Direction(-45.f, 100.f));
	data[Spacecraft::ArmySaucer].directions.push_back(Direction(  0.f,  50.f));
	data[Spacecraft::ArmySaucer].directions.push_back(Direction(+45.f,  50.f));
	data[Spacecraft::ArmySaucer].fireInterval = sf::seconds(2);

	data[Spacecraft::BlackSaucer].hitpoints = 80;
	data[Spacecraft::BlackSaucer].speed = 20.f;
	data[Spacecraft::BlackSaucer].texture = Textures::BlackSaucer;
	data[Spacecraft::BlackSaucer].directions.push_back(Direction(0.f,  30.f));
	data[Spacecraft::BlackSaucer].directions.push_back(Direction(-180.f,  30.f));
	data[Spacecraft::BlackSaucer].fireInterval = sf::seconds(4);


	data[Spacecraft::SpeedSaucer].hitpoints = 10;
	data[Spacecraft::SpeedSaucer].speed = 200.f;
	data[Spacecraft::SpeedSaucer].texture = Textures::SpeedSaucer;
	data[Spacecraft::SpeedSaucer].directions.push_back(Direction(90.f,  500.f));
	data[Spacecraft::SpeedSaucer].fireInterval = sf::seconds(0.8);

	return data;
}
//----------------------------------------------------------------------------------------------------------------------------------------------

std::vector<ProjectileData> initializeProjectileData()
{
	std::vector<ProjectileData> data(Projectile::TypeCount);

	data[Projectile::AlliedBullet].damage = 10;
	data[Projectile::AlliedBullet].speed = 500.f;
	data[Projectile::AlliedBullet].texture = Textures::AlliedBullet;

	data[Projectile::EnemyBullet].damage = 10;
	data[Projectile::EnemyBullet].speed = 300.f;
	data[Projectile::EnemyBullet].texture = Textures::EnemyBullet;

	return data;
}
//----------------------------------------------------------------------------------------------------------------------------------------------

std::vector<PickupData> initializePickupData()
{
	std::vector<PickupData> data(Pickup::TypeCount);

	data[Pickup::HealthRefill].texture = Textures::HealthRefill;
	data[Pickup::HealthRefill].action = [] (Spacecraft& a) { a.repair(20); };

	return data;
}
//----------------------------------------------------------------------------------------------------------------------------------------------
