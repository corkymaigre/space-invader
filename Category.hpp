#ifndef CATEGORY_HPP
#define CATEGORY_HPP
//----------------------------------------------------------------------------------------------------------------------------------------------

// Entity/scene node category, used to dispatch commands
namespace Category
{
	enum Type
	{
		None				= 0,
		SceneSpaceLayer		= 1 << 0,
		PlayerSpacecraft	= 1 << 1,
		AlliedSpacecraft	= 1 << 2,
		EnemySpacecraft		= 1 << 3,
		Pickup				= 1 << 4,
		AlliedProjectile	= 1 << 5,
		EnemyProjectile		= 1 << 6,
		SoundEffect			= 1 << 7,

		Spacecraft = PlayerSpacecraft | AlliedSpacecraft | EnemySpacecraft,
		Projectile = AlliedProjectile | EnemyProjectile,
	};
}
//--------------------------------------------------------------------------------------------------------------------------------------------------

#endif // CATEGORY_HPP
//--------------------------------------------------------------------------------------------------------------------------------------------------