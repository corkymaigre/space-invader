#ifndef RESOURCEHOLDER_HPP
#define RESOURCEHOLDER_HPP
//--------------------------------------------------------------------------------------------------------------------------------------------------

#include <map>
#include <string>
#include <memory>			//Utilisation des unique_ptr
#include <stdexcept>		//Utilisation de 'runtime_error'
#include <cassert>			//gestion des erreurs d'affection avec la macro 'assert'
//--------------------------------------------------------------------------------------------------------------------------------------------------

template <typename Resource, typename Identifier>
class ResourceHolder
{
	public:
		void												load(Identifier id, const std::string& filename);


		Resource&											get(Identifier id);
		const Resource&										get(Identifier id) const;


	private:
		void												insertResource(Identifier id, std::unique_ptr<Resource> resource);


	private:
		std::map<Identifier, std::unique_ptr<Resource>>		mResourceMap;
};
//--------------------------------------------------------------------------------------------------------------------------------------------------

#include "ResourceHolder.inl"
//-------------------------------------------------------------------------------------------------------------------------

#endif // RESOURCEHOLDER_HPP
//-------------------------------------------------------------------------------------------------------------------------
