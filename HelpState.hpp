#ifndef HELPSTATE_HPP
#define HELPSTATE_HPP
//------------------------------------------------------------------------------------------------------------------------------------------------

#include "State.hpp"
#include "Container.hpp"
//------------------------------------------------------------------------------------------------------------------------------------------------

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>
//------------------------------------------------------------------------------------------------------------------------------------------------

class HelpState : public State
{
	public:
											HelpState(StateStack& stack, Context context);

		virtual void						draw();
		virtual bool						update(sf::Time dt);
		virtual bool						handleEvent(const sf::Event& event);


	private:
		sf::Sprite							mBackgroundSprite;
		sf::Text			                mText;
		GUI::Container						mGUIContainer;
};
//------------------------------------------------------------------------------------------------------------------------------------------------

#endif // HELPSTATE_HPP
//------------------------------------------------------------------------------------------------------------------------------------------------
