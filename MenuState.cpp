#include "MenuState.hpp"
#include "Button.hpp"
#include "Utility.hpp"
#include "MusicPlayer.hpp"
#include "ResourceHolder.hpp"
//------------------------------------------------------------------------------------------------------------------------------------------------

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/View.hpp>
//------------------------------------------------------------------------------------------------------------------------------------------------

MenuState::MenuState(StateStack& stack, Context context)
: State(stack, context)
, mGUIContainer()
{
	sf::Texture& texture = context.textures->get(Textures::MenuScreen);
	mBackgroundSprite.setTexture(texture);

	//auto playButton = std::make_shared<GUI::Button>(*context.fonts, *context.textures, *context.sounds);
	auto playButton = std::make_shared<GUI::Button>(context);
	playButton->setPosition(100, 250);
	playButton->setText("Play");
	playButton->setCallback([this] ()
	{
		requestStackPop();
		requestStackPush(States::Game);
	});

	//auto settingsButton = std::make_shared<GUI::Button>(*context.fonts, *context.textures, *context.sounds);
	auto settingsButton = std::make_shared<GUI::Button>(context);
	settingsButton->setPosition(100, 300);
	settingsButton->setText("Settings");
	settingsButton->setCallback([this] ()
	{
		requestStackPush(States::Settings);
	});

	//auto instructionsButton = std::make_shared<GUI::Button>(*context.fonts, *context.textures, *context.sounds);
	auto instructionsButton = std::make_shared<GUI::Button>(context);
	instructionsButton->setPosition(100, 350);
	instructionsButton->setText("Instructions");
	instructionsButton->setCallback([this] ()
	{
		requestStackPush(States::Instructions);
	});

	//auto aboutButton = std::make_shared<GUI::Button>(*context.fonts, *context.textures, *context.sounds);
	auto aboutButton = std::make_shared<GUI::Button>(context);
	aboutButton->setPosition(100, 400);
	aboutButton->setText("About");
	aboutButton->setCallback([this] ()
	{
		requestStackPush(States::About);
	});

	//auto helpButton = std::make_shared<GUI::Button>(*context.fonts, *context.textures, *context.sounds);
	auto helpButton = std::make_shared<GUI::Button>(context);
	helpButton->setPosition(100, 450);
	helpButton->setText("Help");
	helpButton->setCallback([this] ()
	{
		requestStackPush(States::Help);
	});

	//auto exitButton = std::make_shared<GUI::Button>(*context.fonts, *context.textures, *context.sounds);
	auto exitButton = std::make_shared<GUI::Button>(context);
	exitButton->setPosition(100, 500);
	exitButton->setText("Exit");
	exitButton->setCallback([this] ()
	{
		requestStackPop();
	});


	mGUIContainer.pack(playButton);
	mGUIContainer.pack(settingsButton);
	mGUIContainer.pack(instructionsButton);
	mGUIContainer.pack(aboutButton);
	mGUIContainer.pack(helpButton);
	mGUIContainer.pack(exitButton);

	// Play menu theme
	context.music->play(Music::MenuTheme);
}
//------------------------------------------------------------------------------------------------------------------------------------------------

void MenuState::draw()
{
    sf::RenderWindow& window = *getContext().window;

	window.setView(window.getDefaultView());

	sf::RectangleShape backgroundShape;
	backgroundShape.setFillColor(sf::Color(0, 0, 0, 65));
	backgroundShape.setSize(window.getView().getSize());

	window.draw(mBackgroundSprite);
	window.draw(backgroundShape);
	window.draw(mGUIContainer);
}
//------------------------------------------------------------------------------------------------------------------------------------------------

bool MenuState::update(sf::Time)
{
	return true;
}
//------------------------------------------------------------------------------------------------------------------------------------------------

bool MenuState::handleEvent(const sf::Event& event)
{
	mGUIContainer.handleEvent(event);
	return false;
}
//------------------------------------------------------------------------------------------------------------------------------------------------
