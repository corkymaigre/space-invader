; Script de cr�ation du setup.exe
;---------------------------------

#define MyAppName         "Tintin Escaped The Red Planet"
#define MyAppVersion       "9.2"
#define MyAppPublisher    "Corky Maigre"
#define MyAppURL          "http://corkymaigre.zxq.net/"
#define MyAppExeName      "bin.exe"

[Setup]
AppId                 = {{D8DF9B3F-B688-49FB-9146-F72D7799A9E9}
AppName               = {#MyAppName}
AppVersion            = {#MyAppVersion}
;AppVerName           = {#MyAppName} {#MyAppVersion}
AppPublisher          = {#MyAppPublisher}
AppPublisherURL       = {#MyAppURL}
AppSupportURL         = {#MyAppURL}
AppUpdatesURL         = {#MyAppURL}
DefaultDirName        = {pf}\{#MyAppName}
DefaultGroupName      = {#MyAppName}
OutputDir             = C:\Users\Corky\Desktop\Setup
OutputBaseFilename    = setup
SetupIconFile         = C:\Users\Corky\Desktop\Build\Media\Media\icon.ico
Compression           = lzma
SolidCompression      = yes
WizardImageFile       = C:\Users\Corky\Desktop\Build\img1.bmp
WizardSmallImageFile  = C:\Users\Corky\Desktop\Build\img2.bmp

[Languages]
Name: "english";  MessagesFile: "compiler:Default.isl"
Name: "french";   MessagesFile: "compiler:Languages\French.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
Source: "C:\Users\Corky\Desktop\Build\bin.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\Corky\Desktop\Build\libsndfile-1.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\Corky\Desktop\Build\openal32.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\Corky\Desktop\Build\Media\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs

[Icons]
Name: "{group}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
Name: "{group}\{cm:ProgramOnTheWeb,{#MyAppName}}"; Filename: "{#MyAppURL}"
Name: "{group}\{cm:UninstallProgram,{#MyAppName}}"; Filename: "{uninstallexe}"
Name: "{commondesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon

[Run]
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent

