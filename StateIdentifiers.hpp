#ifndef STATEIDENTIFIERS_HPP
#define STATEIDENTIFIERS_HPP
//------------------------------------------------------------------------------------------------------------------------------------------------

namespace States
{
	enum ID
	{
		None,
		Title,
		Menu,
		Game,
		Loading,
		Pause,
		Settings,
		Instructions,
		About,
		Help,
		GameOver
	};
}
//----------------------------------------------------------------------------------------------------------------------------------------------------

#endif // STATEIDENTIFIERS_HPP
//----------------------------------------------------------------------------------------------------------------------------------------------------
