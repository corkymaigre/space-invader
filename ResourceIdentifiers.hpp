#ifndef RESOURCEIDENTIFIERS_HPP
#define RESOURCEIDENTIFIERS_HPP
//----------------------------------------------------------------------------------------------------------------------------------------------

// Forward declaration of SFML classes
namespace sf
{
	class Texture;
	class Font;
	class SoundBuffer;
}
//----------------------------------------------------------------------------------------------------------------------------------------------

namespace Textures
{
	enum ID
	{
		MoonRocket,
		NormalSaucer,
		ArmySaucer,
		BlackSaucer,
		SpeedSaucer,
		AlliedBullet,
		EnemyBullet,
		Ground,
		HealthRefill,
		TitleScreen,
		MenuScreen,
		PauseScreen,
		SettingsScreen,
		InstructionsScreen,
		AboutScreen,
		HelpScreen,
		ButtonNormal,
		ButtonSelected,
		ButtonPressed,
	};
}
//----------------------------------------------------------------------------------------------------------------------------------------------

namespace Fonts
{
	enum ID
	{
		Titles,
		Messages,
		Texts,
	};
}
//----------------------------------------------------------------------------------------------------------------------------------------------

namespace Music
{
	enum ID
	{
		MenuTheme,
		GameTheme,
	};
}
//----------------------------------------------------------------------------------------------------------------------------------------------

namespace SoundEffect
{
	enum ID
	{
		AlliedFire,
		EnemyFire,
		Explosion1,
		Explosion2,
		CollectPickup,
		Button,
		GameOver,
		YouWin,
	};
}
//----------------------------------------------------------------------------------------------------------------------------------------------

// Forward declaration and a few type definitions
template <typename Resource, typename Identifier>
class ResourceHolder;
//------------------------------------------------------------------------------------------------------------------------------------------------

typedef ResourceHolder<sf::Texture, Textures::ID>			TextureHolder;
typedef ResourceHolder<sf::Font, Fonts::ID>					FontHolder;
typedef ResourceHolder<sf::SoundBuffer, SoundEffect::ID>	SoundBufferHolder;
//------------------------------------------------------------------------------------------------------------------------------------------------

#endif // RESOURCEIDENTIFIERS_HPP
//------------------------------------------------------------------------------------------------------------------------------------------------
