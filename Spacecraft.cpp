#include "Spacecraft.hpp"
#include "DataTables.hpp"
#include "Utility.hpp"
#include "Pickup.hpp"
#include "CommandQueue.hpp"
#include "SoundNode.hpp"
#include "ResourceHolder.hpp"
//----------------------------------------------------------------------------------------------------------------------------------------------

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>
//----------------------------------------------------------------------------------------------------------------------------------------------

#include <cmath>
//----------------------------------------------------------------------------------------------------------------------------------------------
using namespace std::placeholders;

namespace
{
	const std::vector<SpacecraftData> Table = initializeSpacecraftData();
}
//----------------------------------------------------------------------------------------------------------------------------------------------

Spacecraft::Spacecraft(Type type, const TextureHolder& textures, const FontHolder& fonts)
: Entity(Table[type].hitpoints)
, mType(type)
, mSprite(textures.get(Table[type].texture))
, mFireCommand()
, mFireCountdown(sf::Time::Zero)
, mIsFiring(false)
, mIsMarkedForRemoval(false)
, mPlayedExplosionSound(false)
, mFireRateLevel(1)
, mSpreadLevel(1)
, mDropPickupCommand()
, mTravelledDistance(0.f)
, mDirectionIndex(0)
, mHealthDisplay(nullptr)
{
	centerOrigin(mSprite);

	mFireCommand.category = Category::SceneSpaceLayer;
	mFireCommand.action   = [this, &textures] (SceneNode& node, sf::Time)
	{
		createBullets(node, textures);
	};

	mDropPickupCommand.category = Category::SceneSpaceLayer;
	mDropPickupCommand.action   = [this, &textures] (SceneNode& node, sf::Time)
	{
		createPickup(node, textures);
	};

	std::unique_ptr<TextNode> healthDisplay(new TextNode(fonts, ""));
	mHealthDisplay = healthDisplay.get();
	attachChild(std::move(healthDisplay));

	updateTexts();
}
//----------------------------------------------------------------------------------------------------------------------------------------------

void Spacecraft::drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(mSprite, states);
}
//----------------------------------------------------------------------------------------------------------------------------------------------

void Spacecraft::updateCurrent(sf::Time dt, CommandQueue& commands)
{
	// Entity has been destroyed: Possibly drop pickup, mark for removal
	if (isDestroyed())
	{
		checkPickupDrop(commands);

		mIsMarkedForRemoval = true;
		return;
	}

	// Check if bullets or missiles are fired
	checkProjectileLaunch(dt, commands);

	// Update enemy movement pattern; apply velocity
	updateMovementPattern(dt);
	Entity::updateCurrent(dt, commands);

	// Update texts
	updateTexts();
}
//----------------------------------------------------------------------------------------------------------------------------------------------

unsigned int Spacecraft::getCategory() const
{
	if (isAllied())
		return Category::PlayerSpacecraft;
	else
		return Category::EnemySpacecraft;
}
//----------------------------------------------------------------------------------------------------------------------------------------------

sf::FloatRect Spacecraft::getBoundingRect() const
{
	return getWorldTransform().transformRect(mSprite.getGlobalBounds());
}
//----------------------------------------------------------------------------------------------------------------------------------------------

bool Spacecraft::isMarkedForRemoval() const
{
	return mIsMarkedForRemoval;
}
//----------------------------------------------------------------------------------------------------------------------------------------------

bool Spacecraft::isAllied() const
{
	return mType == MoonRocket;
}
//----------------------------------------------------------------------------------------------------------------------------------------------

float Spacecraft::getMaxSpeed() const
{
	return Table[mType].speed;
}
//----------------------------------------------------------------------------------------------------------------------------------------------

void Spacecraft::fire()
{
	// Only ships with fire interval != 0 are able to fire
	if (Table[mType].fireInterval != sf::Time::Zero)
		mIsFiring = true;
}
//----------------------------------------------------------------------------------------------------------------------------------------------

void Spacecraft::playLocalSound(CommandQueue& commands, SoundEffect::ID effect)
{
	Command command;
	command.category = Category::SoundEffect;
	command.action = derivedAction<SoundNode>(
		std::bind(&SoundNode::playSound, _1, effect, getWorldPosition()));

	commands.push(command);
}
//----------------------------------------------------------------------------------------------------------------------------------------------

void Spacecraft::updateMovementPattern(sf::Time dt)
{
	// Enemy airplane: Movement pattern
	const std::vector<Direction>& directions = Table[mType].directions;
	if (!directions.empty())
	{
		// Moved long enough in current direction: Change direction
		float distanceToTravel = directions[mDirectionIndex].distance;
		if (mTravelledDistance > distanceToTravel)
		{
			mDirectionIndex = (mDirectionIndex + 1) % directions.size(); // allows a cycle
			mTravelledDistance = 0.f;
		}

		// Compute velocity from direction
		float radians = toRadian(directions[mDirectionIndex].angle + 90.f);
		float vx = getMaxSpeed() * std::cos(radians);
		float vy = getMaxSpeed() * std::sin(radians);

		setVelocity(vx, vy);

		mTravelledDistance += getMaxSpeed() * dt.asSeconds();
	}
}
//----------------------------------------------------------------------------------------------------------------------------------------------

void Spacecraft::checkPickupDrop(CommandQueue& commands)
{
	if (!isAllied() && randomInt(3) == 0)
		commands.push(mDropPickupCommand);
}
//----------------------------------------------------------------------------------------------------------------------------------------------

void Spacecraft::checkProjectileLaunch(sf::Time dt, CommandQueue& commands)
{
	// Enemies try to fire all the time
	if (!isAllied())
		fire();

	// Check for automatic gunfire, allow only in intervals
	if (mIsFiring && mFireCountdown <= sf::Time::Zero)
	{
		// Interval expired: We can fire a new bullet
		commands.push(mFireCommand);
		playLocalSound(commands, isAllied() ? SoundEffect::AlliedFire : SoundEffect::EnemyFire);
		mFireCountdown += Table[mType].fireInterval / (mFireRateLevel + 1.f);
		mIsFiring = false;
	}
	else if (mFireCountdown > sf::Time::Zero)
	{
		// Interval not expired: Decrease it further
		mFireCountdown -= dt;
		mIsFiring = false;
	}
}
//----------------------------------------------------------------------------------------------------------------------------------------------

void Spacecraft::createBullets(SceneNode& node, const TextureHolder& textures) const
{
	Projectile::Type type = isAllied() ? Projectile::AlliedBullet : Projectile::EnemyBullet;

	switch (mSpreadLevel)
	{
		case 1:
			createProjectile(node, type, 0.0f, 0.5f, textures);
			break;

		case 2:
			createProjectile(node, type, -0.33f, 0.33f, textures);
			createProjectile(node, type, +0.33f, 0.33f, textures);
			break;

		case 3:
			createProjectile(node, type, -0.5f, 0.33f, textures);
			createProjectile(node, type,  0.0f, 0.5f, textures);
			createProjectile(node, type, +0.5f, 0.33f, textures);
			break;
	}
}
//----------------------------------------------------------------------------------------------------------------------------------------------

void Spacecraft::createProjectile(SceneNode& node, Projectile::Type type, float xOffset, float yOffset, const TextureHolder& textures) const
{
	std::unique_ptr<Projectile> projectile(new Projectile(type, textures));

	sf::Vector2f offset(xOffset * mSprite.getGlobalBounds().width, yOffset * mSprite.getGlobalBounds().height);
	sf::Vector2f velocity(0, projectile->getMaxSpeed());

	float sign = isAllied() ? -1.f : +1.f;
	projectile->setPosition(getWorldPosition() + offset * sign);
	projectile->setVelocity(velocity * sign);
	node.attachChild(std::move(projectile));
}
//----------------------------------------------------------------------------------------------------------------------------------------------

void Spacecraft::createPickup(SceneNode& node, const TextureHolder& textures) const
{
	auto type = static_cast<Pickup::Type>(randomInt(Pickup::TypeCount));

	std::unique_ptr<Pickup> pickup(new Pickup(type, textures));
	pickup->setPosition(getWorldPosition());
	pickup->setVelocity(0.f, 1.f);
	node.attachChild(std::move(pickup));
}
//----------------------------------------------------------------------------------------------------------------------------------------------

void Spacecraft::updateTexts()
{
	mHealthDisplay->setString(toString(getHitpoints()) + " HP");
	mHealthDisplay->setPosition(0.f, 40.f);
	mHealthDisplay->setRotation(-getRotation());
}
//----------------------------------------------------------------------------------------------------------------------------------------------
