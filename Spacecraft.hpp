#ifndef SPACECRAFT_HPP
#define SPACECRAFT_HPP
//----------------------------------------------------------------------------------------------------------------------------------------------

#include "Entity.hpp"
#include "Command.hpp"
#include "ResourceIdentifiers.hpp"
#include "Projectile.hpp"
#include "TextNode.hpp"
//----------------------------------------------------------------------------------------------------------------------------------------------

#include <SFML/Graphics/Sprite.hpp>
//----------------------------------------------------------------------------------------------------------------------------------------------

class Spacecraft : public Entity
{
	public:
		enum Type
		{
			MoonRocket,
			NormalSaucer,
			ArmySaucer,
			BlackSaucer,
			SpeedSaucer,
			TypeCount
		};


	public:
											Spacecraft(Type type, const TextureHolder& textures, const FontHolder& fonts);

		virtual void						drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;
		virtual void 						updateCurrent(sf::Time dt, CommandQueue& commands);
		virtual unsigned int				getCategory() const;
		virtual sf::FloatRect				getBoundingRect() const;

		virtual bool 						isMarkedForRemoval() const;
		bool								isAllied() const;
		float								getMaxSpeed() const;

		void 								fire();
		void								playLocalSound(CommandQueue& commands, SoundEffect::ID effect);

	private:
		void								updateMovementPattern(sf::Time dt);
		void								checkPickupDrop(CommandQueue& commands);
		void								checkProjectileLaunch(sf::Time dt, CommandQueue& commands);

		void								createBullets(SceneNode& node, const TextureHolder& textures) const;
		void								createProjectile(SceneNode& node, Projectile::Type type, float xOffset, float yOffset, const TextureHolder& textures) const;
		void								createPickup(SceneNode& node, const TextureHolder& textures) const;

		void								updateTexts();


	private:
		Type								mType;
		sf::Sprite							mSprite;
		Command 							mFireCommand;
		sf::Time							mFireCountdown;
		bool 								mIsFiring;
		bool 								mIsMarkedForRemoval;

		bool								mPlayedExplosionSound;

		int									mFireRateLevel;
		int									mSpreadLevel;

		Command 							mDropPickupCommand;
		float								mTravelledDistance;
		std::size_t							mDirectionIndex;
		TextNode*							mHealthDisplay;
};
//----------------------------------------------------------------------------------------------------------------------------------------------

#endif // SPACECRAFT_HPP
//--------------------------------------------------------------------------------------------------------------------------------------------------
