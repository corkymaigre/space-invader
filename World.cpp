#include "DefineConstants.hpp"
#include "World.hpp"
#include "Projectile.hpp"
#include "Pickup.hpp"
#include "TextNode.hpp"
#include "SoundNode.hpp"
//----------------------------------------------------------------------------------------------------------------------------------------------

#include <SFML/Graphics/RenderWindow.hpp>
//----------------------------------------------------------------------------------------------------------------------------------------------

#include <algorithm>
#include <cmath>
#include <limits>
//----------------------------------------------------------------------------------------------------------------------------------------------

World::World(sf::RenderWindow& window, FontHolder& fonts, SoundPlayer& sounds)
: mWindow(window)
, mWorldView(window.getDefaultView())
, mTextures()
, mFonts(fonts)
, mSounds(sounds)
, mSceneGraph()
, mSceneLayers()
, mWorldBounds(
	0.f,								// left X position
	0.f, 								// top Y position
	mWorldView.getSize().x, 			// width
	5500.f)
, mSpawnPosition(
	mWorldView.getSize().x / 2.f,
	mWorldBounds.height - mWorldView.getSize().y / 2.f)
, mScrollSpeed(SCROLL_SPEED)
, mPlayerSpacecraft(nullptr)
, mEnemySpawnPoints()
, mActiveEnemies()
{
	loadTextures();
	buildScene();

	// Prepare the view
	mWorldView.setCenter(mSpawnPosition);
}
//----------------------------------------------------------------------------------------------------------------------------------------------

void World::update(sf::Time dt)
{
	// Scroll the world
	mWorldView.move( 0.f , mScrollSpeed * dt.asSeconds());

	// Reset player velocity
	mPlayerSpacecraft->setVelocity(0.f, 0.f);

	// Setup commands to destroy entities
	destroyEntitiesOutsideView();

	// Forward commands to scene graph, adapt velocity (scrolling, diagonal correction)
	while (!mCommandQueue.isEmpty())
		mSceneGraph.onCommand(mCommandQueue.pop(), dt);

	// Adapt velocity (scrolling, diagonal correction)
	// Up and Right together gives a velocity higher than Up or Right individually --> divide by sqrt(2)
	adaptPlayerVelocity();

	// Collision detection and response (may destroy entities)
	handleCollisions();

	// Remove all destroyed entities, create new ones
	mSceneGraph.removeWrecks();
	spawnEnemies();

	// Regular update step, adapt position (correct if outside view)
	mSceneGraph.update(dt, mCommandQueue);
	adaptPlayerPosition();

	updateSounds();
}
//----------------------------------------------------------------------------------------------------------------------------------------------

void World::draw()
{
	mWindow.setView(mWorldView);
	mWindow.draw(mSceneGraph);
}
//----------------------------------------------------------------------------------------------------------------------------------------------

CommandQueue& World::getCommandQueue()
{
	return mCommandQueue;
}
//----------------------------------------------------------------------------------------------------------------------------------------------

bool World::hasAlivePlayer() const
{
	return !mPlayerSpacecraft->isMarkedForRemoval();
}
//----------------------------------------------------------------------------------------------------------------------------------------------

bool World::hasPlayerReachedEnd() const
{
	return !mWorldBounds.contains(mPlayerSpacecraft->getPosition());
}
//----------------------------------------------------------------------------------------------------------------------------------------------

void World::loadTextures()
{
	mTextures.load(Textures::MoonRocket, MOON_ROCKET);
	mTextures.load(Textures::NormalSaucer, NORMAL_SAUCER);
	mTextures.load(Textures::ArmySaucer, ARMY_SAUCER);
	mTextures.load(Textures::BlackSaucer, BLACK_SAUCER);
	mTextures.load(Textures::SpeedSaucer, SPEED_SAUCER);

	mTextures.load(Textures::Ground, GROUND);

	mTextures.load(Textures::AlliedBullet, ALLIED_BULLET);
	mTextures.load(Textures::EnemyBullet, ENEMY_BULLET);

	mTextures.load(Textures::HealthRefill, HEALTH_REFILL);
}
//----------------------------------------------------------------------------------------------------------------------------------------------

void World::adaptPlayerPosition()
{
	// Keep player's position inside the screen bounds, at least borderDistance units from the border
	sf::FloatRect viewBounds(
		mWorldView.getCenter() - mWorldView.getSize() / 2.f,
		mWorldView.getSize());
	// Or sf::FloatRect viewBounds = getViewBounds();
	const float borderDistance = 35.f;

	sf::Vector2f position = mPlayerSpacecraft->getPosition();
	position.x = std::max(position.x, viewBounds.left + borderDistance);
	position.x = std::min(position.x, viewBounds.left + viewBounds.width - borderDistance);
	position.y = std::max(position.y, viewBounds.top + borderDistance);
	position.y = std::min(position.y, viewBounds.top + viewBounds.height - borderDistance);
	mPlayerSpacecraft->setPosition(position);
}
//----------------------------------------------------------------------------------------------------------------------------------------------

void World::adaptPlayerVelocity()
{
	sf::Vector2f velocity = mPlayerSpacecraft->getVelocity();

	// If moving diagonally, reduce velocity (to have always same velocity)
	if (velocity.x != 0.f && velocity.y != 0.f)
		mPlayerSpacecraft->setVelocity(velocity / std::sqrt(2.f));

	// Add scrolling velocity
	mPlayerSpacecraft->accelerate(0.f, mScrollSpeed);
}
//----------------------------------------------------------------------------------------------------------------------------------------------

bool matchesCategories(SceneNode::Pair& colliders, Category::Type type1, Category::Type type2)
{
	unsigned int category1 = colliders.first->getCategory();
	unsigned int category2 = colliders.second->getCategory();

	// Make sure first pair entry has category type1 and second has type2
	if (type1 & category1 && type2 & category2)
	{
		return true;
	}
	else if (type1 & category2 && type2 & category1)
	{
		std::swap(colliders.first, colliders.second);
		return true;
	}
	else
	{
		return false;
	}
}
//----------------------------------------------------------------------------------------------------------------------------------------------

void World::handleCollisions()
{
	std::set<SceneNode::Pair> collisionPairs;
	mSceneGraph.checkSceneCollision(mSceneGraph, collisionPairs);

    // FOREACH
	for (SceneNode::Pair pair: collisionPairs)
	{
		if (matchesCategories(pair, Category::PlayerSpacecraft, Category::EnemySpacecraft))
		{
			auto& player = static_cast<Spacecraft&>(*pair.first);
			auto& enemy = static_cast<Spacecraft&>(*pair.second);

			// Collision: Player damage = enemy's remaining HP
			player.damage(enemy.getHitpoints());
			enemy.destroy();
		}

		else if (matchesCategories(pair, Category::PlayerSpacecraft, Category::Pickup))
		{
			auto& player = static_cast<Spacecraft&>(*pair.first);
			auto& pickup = static_cast<Pickup&>(*pair.second);

			// Apply pickup effect to player, destroy projectile
			pickup.apply(player);
			pickup.destroy();
			player.playLocalSound(mCommandQueue, SoundEffect::CollectPickup);
		}

		else if (matchesCategories(pair, Category::EnemySpacecraft, Category::AlliedProjectile)
			  || matchesCategories(pair, Category::PlayerSpacecraft, Category::EnemyProjectile))
		{
			auto& spacecraft = static_cast<Spacecraft&>(*pair.first);
			auto& projectile = static_cast<Projectile&>(*pair.second);

			// Apply projectile damage to aircraft, destroy projectile
			spacecraft.damage(projectile.getDamage());
			projectile.destroy();
		}
	}
}
//----------------------------------------------------------------------------------------------------------------------------------------------

void World::updateSounds()
{
	// Set listener's position to player position
	mSounds.setListenerPosition(mPlayerSpacecraft->getWorldPosition());

	// Remove unused sounds
	mSounds.removeStoppedSounds();
}
//----------------------------------------------------------------------------------------------------------------------------------------------

void World::buildScene()
{
	// Initialize the different layers
	for (std::size_t i = 0; i < LayerCount; ++i)
	{
		Category::Type category = (i == Space) ? Category::SceneSpaceLayer : Category::None;

		SceneNode::Ptr layer(new SceneNode(category));
		mSceneLayers[i] = layer.get();

		mSceneGraph.attachChild(std::move(layer));
	}


	// Prepare the tiled background
	sf::Texture& texture = mTextures.get(Textures::Ground);
	sf::IntRect textureRect(mWorldBounds);
	texture.setRepeated(true);


	// Add the background sprite to the scene
	std::unique_ptr<SpriteNode> backgroundSprite(
		new SpriteNode(texture, textureRect));
	backgroundSprite->setPosition(
		mWorldBounds.left,
		mWorldBounds.top);
	mSceneLayers[Background]
		->attachChild(std::move(backgroundSprite));

	// Add sound effect node
	std::unique_ptr<SoundNode> soundNode(new SoundNode(mSounds));
	mSceneGraph.attachChild(std::move(soundNode));

	// Add player's aircraft
	std::unique_ptr<Spacecraft> player(
		new Spacecraft(Spacecraft::MoonRocket, mTextures, mFonts));
	mPlayerSpacecraft = player.get();
	mPlayerSpacecraft->setPosition(mSpawnPosition);
	mSceneLayers[Space]->attachChild(std::move(player));


	// Add enemy aircraft
	addEnemies();
}
//----------------------------------------------------------------------------------------------------------------------------------------------

void World::addEnemies()
{
	// Add enemies to the spawn point container
	addEnemy(Spacecraft::NormalSaucer,     -50.f,  500.f);
	addEnemy(Spacecraft::NormalSaucer,      50.f,  500.f);
	addEnemy(Spacecraft::NormalSaucer,    -150.f,  525.f);
	addEnemy(Spacecraft::NormalSaucer,     150.f,  525.f);
	addEnemy(Spacecraft::NormalSaucer,    -250.f,  550.f);
	addEnemy(Spacecraft::NormalSaucer,     250.f,  550.f);
	addEnemy(Spacecraft::NormalSaucer,    -350.f,  575.f);
	addEnemy(Spacecraft::NormalSaucer,     350.f,  575.f);

    addEnemy(Spacecraft::SpeedSaucer,      350.f, 675.f);
    addEnemy(Spacecraft::SpeedSaucer,      350.f, 805.f);


	addEnemy(Spacecraft::ArmySaucer,      -300.f, 950.f);
	addEnemy(Spacecraft::ArmySaucer,        20.f, 950.f);
	addEnemy(Spacecraft::ArmySaucer,      -150.f, 975.f);
	addEnemy(Spacecraft::ArmySaucer,       250.f, 1025.f);
	addEnemy(Spacecraft::ArmySaucer,        -5.f, 1100.f);
	addEnemy(Spacecraft::ArmySaucer,       300.f, 1150.f);
    addEnemy(Spacecraft::ArmySaucer,      -100.f, 1225.f);
    addEnemy(Spacecraft::ArmySaucer,      350.f, 1000.f);

    addEnemy(Spacecraft::BlackSaucer,     100.f, 1400.f);
    addEnemy(Spacecraft::SpeedSaucer,      350.f, 1425.f);
    addEnemy(Spacecraft::BlackSaucer,     225.f, 1500.f);

	addEnemy(Spacecraft::BlackSaucer,     -200.f, 1700.f);
	addEnemy(Spacecraft::BlackSaucer,       60.f, 1775.f);
	addEnemy(Spacecraft::SpeedSaucer,      350.f, 1800.f);
	addEnemy(Spacecraft::BlackSaucer,       20.f, 1875.f);
	addEnemy(Spacecraft::BlackSaucer,       200.f, 1825.f);

    addEnemy(Spacecraft::NormalSaucer,     -50.f,  2100.f);
	addEnemy(Spacecraft::NormalSaucer,      50.f,  2100.f);
	addEnemy(Spacecraft::NormalSaucer,    -150.f,  2125.f);
	addEnemy(Spacecraft::NormalSaucer,     150.f,  2125.f);
	addEnemy(Spacecraft::NormalSaucer,    -250.f,  2150.f);
	addEnemy(Spacecraft::NormalSaucer,     250.f,  2150.f);
	addEnemy(Spacecraft::NormalSaucer,    -350.f,  2200.f);
	addEnemy(Spacecraft::NormalSaucer,     350.f,  2200.f);

    addEnemy(Spacecraft::ArmySaucer,      80.f, 2500.f);
	addEnemy(Spacecraft::ArmySaucer,        -180.f, 2525.f);
	addEnemy(Spacecraft::ArmySaucer,      320.f, 2525.f);
	addEnemy(Spacecraft::ArmySaucer,       20.f, 2550.f);
	addEnemy(Spacecraft::ArmySaucer,       140, 2575.f);
	addEnemy(Spacecraft::ArmySaucer,       -80.f, 2600.f);
    addEnemy(Spacecraft::ArmySaucer,      -275.f, 2575.f);

	addEnemy(Spacecraft::SpeedSaucer, 350.f, 2650.f);
	addEnemy(Spacecraft::SpeedSaucer, 475.f, 2680.f);

	addEnemy(Spacecraft::BlackSaucer,     -350.f, 3000.f);
	addEnemy(Spacecraft::BlackSaucer,       -100.f, 3100.f);
	addEnemy(Spacecraft::SpeedSaucer,      500.f, 3125.f);
	addEnemy(Spacecraft::BlackSaucer,       150.f, 3050.f);
	addEnemy(Spacecraft::BlackSaucer,       350.f, 3000.f);

	addEnemy(Spacecraft::SpeedSaucer, 350.f, 2950.f);

	addEnemy(Spacecraft::NormalSaucer,     -50.f,  3400.f);
	addEnemy(Spacecraft::NormalSaucer,      50.f,  3400.f);
	addEnemy(Spacecraft::NormalSaucer,    -150.f,  3375.f);
	addEnemy(Spacecraft::NormalSaucer,     150.f,  3375.f);
	addEnemy(Spacecraft::NormalSaucer,    -250.f,  3350.f);
	addEnemy(Spacecraft::NormalSaucer,     250.f,  3350.f);
	addEnemy(Spacecraft::NormalSaucer,    -350.f,  3325.f);
	addEnemy(Spacecraft::NormalSaucer,     350.f,  3325.f);

	addEnemy(Spacecraft::ArmySaucer,      0.f, 3600.f);
	addEnemy(Spacecraft::ArmySaucer,        -200.f, 3575.f);
	addEnemy(Spacecraft::ArmySaucer,      350.f, 3625.f);
	addEnemy(Spacecraft::ArmySaucer,      -350.f, 3600.f);
	addEnemy(Spacecraft::ArmySaucer,      100.f, 3675.f);
	addEnemy(Spacecraft::ArmySaucer,      -350.f, 3675.f);

    addEnemy(Spacecraft::BlackSaucer,       -350.f, 3850.f);
	addEnemy(Spacecraft::BlackSaucer,       -200.f, 3850.f);
	addEnemy(Spacecraft::BlackSaucer,       0.f, 3850.f);
	addEnemy(Spacecraft::BlackSaucer,       200.f, 3850.f);
	addEnemy(Spacecraft::BlackSaucer,       350.f, 3850.f);

	addEnemy(Spacecraft::SpeedSaucer, 350.f, 3700.f);

	addEnemy(Spacecraft::NormalSaucer,    -300.f,  4200.f);
	addEnemy(Spacecraft::NormalSaucer,     300.f,  4200.f);
	addEnemy(Spacecraft::NormalSaucer,    -200.f,  4150.f);
	addEnemy(Spacecraft::NormalSaucer,     200.f,  4150.f);
	addEnemy(Spacecraft::NormalSaucer,    -100.f,  4100.f);
	addEnemy(Spacecraft::NormalSaucer,     100.f,  4100.f);

    addEnemy(Spacecraft::SpeedSaucer, 350.f, 4300.f);
    addEnemy(Spacecraft::SpeedSaucer, 350.f, 4400.f);

    addEnemy(Spacecraft::ArmySaucer,      -50.f, 4600.f);
	addEnemy(Spacecraft::ArmySaucer,        -150.f, 4650.f);
	addEnemy(Spacecraft::ArmySaucer,        250.f, 4625.f);
	addEnemy(Spacecraft::ArmySaucer,        350.f, 4675.f);

	addEnemy(Spacecraft::SpeedSaucer, 350.f, 4650.f);

	addEnemy(Spacecraft::NormalSaucer,     -50.f,  4825.f);
	addEnemy(Spacecraft::NormalSaucer,      50.f,  4825.f);
	addEnemy(Spacecraft::NormalSaucer,    -150.f,  4850.f);
	addEnemy(Spacecraft::NormalSaucer,     150.f,  4850.f);
	addEnemy(Spacecraft::NormalSaucer,    -250.f,  4875.f);
	addEnemy(Spacecraft::NormalSaucer,     250.f,  4875.f);
	addEnemy(Spacecraft::NormalSaucer,    -350.f,  4900.f);
	addEnemy(Spacecraft::NormalSaucer,     350.f,  4900.f);

    addEnemy(Spacecraft::SpeedSaucer, 350.f, 5000.f);

	// Sort all enemies according to their y value, such that lower enemies are checked first for spawning
	std::sort(mEnemySpawnPoints.begin(), mEnemySpawnPoints.end(), [] (SpawnPoint lhs, SpawnPoint rhs)
	{
		return lhs.y < rhs.y;
	});
}
//----------------------------------------------------------------------------------------------------------------------------------------------

void World::addEnemy(Spacecraft::Type type, float relX, float relY)
{
	SpawnPoint spawn(type, mSpawnPosition.x + relX, mSpawnPosition.y - relY);
	mEnemySpawnPoints.push_back(spawn);
}
//----------------------------------------------------------------------------------------------------------------------------------------------

void World::spawnEnemies()
{
	// Spawn all enemies entering the view area (including distance) this frame
	while (!mEnemySpawnPoints.empty()
		&& mEnemySpawnPoints.back().y > getBattlefieldBounds().top)
	{
		SpawnPoint spawn = mEnemySpawnPoints.back();

		std::unique_ptr<Spacecraft> enemy(new Spacecraft(spawn.type, mTextures, mFonts));
		enemy->setPosition(spawn.x, spawn.y);
		enemy->setRotation(0.f);

		mSceneLayers[Space]->attachChild(std::move(enemy));

		// Enemy is spawned, remove from the list to spawn
		mEnemySpawnPoints.pop_back();
	}
}
//----------------------------------------------------------------------------------------------------------------------------------------------

void World::destroyEntitiesOutsideView()
{
	Command command;
	command.category = Category::Projectile | Category::EnemySpacecraft;
	command.action = derivedAction<Entity>([this] (Entity& e, sf::Time)
	{
		if (!getBattlefieldBounds().intersects(e.getBoundingRect()))
			e.destroy();
	});

	mCommandQueue.push(command);
}
//----------------------------------------------------------------------------------------------------------------------------------------------

sf::FloatRect World::getViewBounds() const
{
	return sf::FloatRect(mWorldView.getCenter() - mWorldView.getSize() / 2.f, mWorldView.getSize());
}
//----------------------------------------------------------------------------------------------------------------------------------------------

sf::FloatRect World::getBattlefieldBounds() const
{
	// Return view bounds + some area at top, where enemies spawn
	sf::FloatRect bounds = getViewBounds();
	bounds.top -= 100.f;
	bounds.height += 100.f;

	return bounds;
}
//----------------------------------------------------------------------------------------------------------------------------------------------
