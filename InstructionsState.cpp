#include "InstructionsState.hpp"
#include "Button.hpp"
#include "Utility.hpp"
#include "ResourceHolder.hpp"
//------------------------------------------------------------------------------------------------------------------------------------------------

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/View.hpp>
//------------------------------------------------------------------------------------------------------------------------------------------------

InstructionsState::InstructionsState(StateStack& stack, Context context)
: State(stack, context)
, mBackgroundSprite()
, mInstructionsText()
, mGUIContainer()
{
	sf::Texture& texture = context.textures->get(Textures::InstructionsScreen);
	sf::Vector2f windowSize(context.window->getSize());

	mBackgroundSprite.setTexture(texture);

	mInstructionsText.setFont(context.fonts->get(Fonts::Texts));
	mInstructionsText.setString("You control the Tintin's moonrocket and you want\n"
                    "to escape the red planet.\n"
                    "Enemies are coming around, you must fight them and\n"
                    "avoid their bullets with intelligence to survive.\n"
                    "\nThere is no points, no scores, you just have to\n"
                    "try to achieve the game.\n"
                    "\nUsing the key pad, move around and kill\n"
                    "pressing space bar.");
    mInstructionsText.setCharacterSize(28);
	centerOrigin(mInstructionsText);
	mInstructionsText.setPosition(0.5f * windowSize.x, 0.4f * windowSize.y);

	//auto backButton = std::make_shared<GUI::Button>(*context.fonts, *context.textures, *context.sounds);
	auto backButton = std::make_shared<GUI::Button>(context);
	backButton->setPosition(WIN_WIDTH / 2, 450.f);
	backButton->setText("Back to menu");
	backButton->setCallback(std::bind(&InstructionsState::requestStackPop, this));

	// Or
	/*backButton->setCallback([this] ()
	{
		requestStackPop();
	});*/


	mGUIContainer.pack(backButton);
}
//------------------------------------------------------------------------------------------------------------------------------------------------

void InstructionsState::draw()
{
	sf::RenderWindow& window = *getContext().window;
	window.setView(window.getDefaultView());

	sf::RectangleShape backgroundShape;
	backgroundShape.setFillColor(sf::Color(0, 0, 0, 180));
	backgroundShape.setSize(window.getView().getSize());

	window.draw(mBackgroundSprite);
	window.draw(backgroundShape);
	window.draw(mInstructionsText);
	window.draw(mGUIContainer);
}
//------------------------------------------------------------------------------------------------------------------------------------------------

bool InstructionsState::update(sf::Time)
{
	return true;
}
//------------------------------------------------------------------------------------------------------------------------------------------------

bool InstructionsState::handleEvent(const sf::Event& event)
{
	mGUIContainer.handleEvent(event);
	return false;
}
//------------------------------------------------------------------------------------------------------------------------------------------------
