#ifndef PLAYER_HPP
#define PLAYER_HPP
//--------------------------------------------------------------------------------------------------------------------------------------------------

#include "Command.hpp"
#include "DefineConstants.hpp"
//--------------------------------------------------------------------------------------------------------------------------------------------------

#include <SFML/Window/Event.hpp>
//--------------------------------------------------------------------------------------------------------------------------------------------------

#include <map>
//--------------------------------------------------------------------------------------------------------------------------------------------------

class CommandQueue;

class Player
{
	public:
		enum Action
		{
			MoveLeft,
			MoveRight,
			MoveUp,
			MoveDown,
			Fire,
			ActionCount
		};

		enum GameStatus
		{
			GameRunning,
			GameSuccess,
			GameFailure
		};

	public:
													Player();

		void										handleEvent(const sf::Event& event, CommandQueue& commands);
		void										handleRealtimeInput(CommandQueue& commands);

		void										assignKey(Action action, sf::Keyboard::Key key);
		sf::Keyboard::Key							getAssignedKey(Action action) const;

		void 										setGameStatus(GameStatus status);
		GameStatus 								    getGameStatus() const;


	private:
		void										initializeActions();
		static bool									isRealtimeAction(Action action);


	private:
		std::map<sf::Keyboard::Key, Action>			mKeyBinding;
		std::map<Action, Command>					mActionBinding;
		GameStatus 								    mCurrentGameStatus;
};
//--------------------------------------------------------------------------------------------------------------------------------------------------

#endif // PLAYER_HPP
//--------------------------------------------------------------------------------------------------------------------------------------------------
